# Fay - Terminal image rasteriser

Draws an image in the terminal using block characters. Requires terminal emulator with 24-bit colour support.

Usage: `fay [options] filename`

| OPTIONS | | |
|---------|----------|----------------|
| `--width` _n_ | `-w` _n_ | specify the width to which to downsample the picture. If not specified, image will be downsampled to fit the terminal window or, if stdout redirected, full size of the image will be used. |
| `--escape` | `-e` | print the escape sequences. |

Compile for GNU/Linux using gcc:

```
g++ $(Magick++-config --cxxflags --cppflags) -O3 -std=gnu++17 -o fay fay.cpp $(Magick++-config --ldflags --libs)
```

Dependency: ImageMagick's Magick++ API


