#include <iostream>
#include <exception>

#include <unistd.h>
#include <sys/ioctl.h>
#include <getopt.h>
#include <Magick++.h>

using string_t = std::string;
using ERROR = std::runtime_error;

class PixelPairTC {
  string_t bg_escseq = "0";
  string_t fg_escseq;
  string_t bg_escseq_old;
  string_t fg_escseq_old;
  string_t character;
  
public:
  string_t CSI = "\033[";
  string_t NL = "\n";
  
  void reset() {
    bg_escseq = "0";
    fg_escseq.clear();
    bg_escseq_old.clear();
    fg_escseq_old.clear();
    character.clear();
  }
  void set(uint8_t r0, uint8_t g0, uint8_t b0,
           uint8_t r1, uint8_t g1, uint8_t b1) {

    fg_escseq_old = fg_escseq;
    bg_escseq_old = bg_escseq;
    
    string_t    top_escseq = std::to_string(r0) + ";"
                           + std::to_string(g0) + ";"
                           + std::to_string(b0);
    string_t bottom_escseq = std::to_string(r1) + ";"
                           + std::to_string(g1) + ";"
                           + std::to_string(b1);
    // transparency 
    // both pixels transparent
    if ((r0 == 0 and g0 == 0 and b0 == 0) and 
        (r1 == 0 and r1 == 0 and r1 == 0)
    ) {
      character = " ";
      bg_escseq = "0";
      return;
    }
    // top pixel transparent, bottom isn't
    if (r0 == 0 and g0 == 0 and b0 == 0) {
      character = "▄";
      fg_escseq = bottom_escseq;
      bg_escseq = "0";
      return;
    }
    // bottom pixel transparent, top isn't
    if (r1 == 0 and g1 == 0 and b1 == 0) {
      character = "▀";
      fg_escseq = top_escseq;
      bg_escseq = "0";
      return;
    }
    // pixels not transparent
    if (top_escseq == bottom_escseq) {
      if (top_escseq == fg_escseq) {
        character = "█";
        return;
      }
      if (top_escseq == bg_escseq) {
        character = " ";
        return;
      }
      character = "█";
      fg_escseq = top_escseq;
      return;
    }
    if (top_escseq == fg_escseq) {
      character = "▀";
      bg_escseq = bottom_escseq;
      return;
    }
    if (top_escseq == bg_escseq) {
      character = "▄";
      fg_escseq = bottom_escseq;
      return;
    }  
    if (bottom_escseq == fg_escseq) {
      character = "▄";
      bg_escseq = top_escseq;
      return;
    }
    if (bottom_escseq == bg_escseq) {
      character = "▀";
      fg_escseq = top_escseq;
      return;
    }
    // general case
    character = "▀";
    fg_escseq = top_escseq;
    bg_escseq = bottom_escseq;
  }
  
  string_t get() {
    if (bg_escseq == fg_escseq_old and
        fg_escseq == bg_escseq_old and
        bg_escseq != "0"    and
        fg_escseq != ""     and
        fg_escseq_old != "" and
        bg_escseq_old != "" and
        character == "▀"
    ) {
      return "▄";
    }
    
    string_t out;
    bool fg_set = false;
    
    if (bg_escseq != bg_escseq_old) {
      if (bg_escseq == "0") {
        out += CSI + "0m";
        fg_escseq_old.clear();
        
        if (character == "▀" or character == "█" or character == "▄") {
          out += CSI + "38;2;" + fg_escseq + "m";
          fg_set = true;
        }
      }
      else {
        out += CSI + "48;2;" + bg_escseq + "m";
      }
    }
    
    if (!fg_set and fg_escseq != fg_escseq_old) {
      out += CSI + "38;2;" + fg_escseq + "m";
    }
    
    out += character;
    return out;
  }
};

void detectTrueColor() {
  const char* env_colorterm = getenv("COLORTERM");
  const char* env_term = getenv("TERM");
  if ( (env_colorterm != nullptr and string_t(env_colorterm) == "truecolor")
    or (env_term != nullptr and string_t(env_term).substr(0,8) == "xterm-22") )
  { 
    return; // truecolor support detected
  }
  if (env_term == nullptr and env_colorterm == nullptr) {
    throw ERROR("Environment variable TERM not set. Could not detect terminal colour support.");
  }
  throw ERROR("24-bit colour suport not detected. To force it, set COLORTERM=truecolor");
}

std::pair<size_t, size_t> getTerminalSize() {
  winsize size;
  ioctl(STDOUT_FILENO, TIOCGWINSZ, &size);
  return std::make_pair(size.ws_row, size.ws_col);
}
/******************************************************************************/
void printhelp() {
  std::cout 
  << "Fay, terminal image rasteriser\n\n"
  << "Usage: fay [options] filename\n\n"
  << "OPTIONS\n"
  << " --width n  -w n  specify the width to which to downsample the picture.\n"
  << "                  If not specified, image will be downsampled to fit the\n"
  << "                  terminal window or, if stdout redirected, full size of\n"
  << "                  the image will be used.\n"
  << " --escape   -e    print the escape sequences.\n"
  << " --help     -h    show this help text.\n"
  << " --version  -v    show version.\n\n"
  << "Fay requires the terminal emulator to support 24-bit colour output, and\n"
  << "checks the COLORTERM and/or TERM environment variables.\n"
  << "\"Fay\" is an old English word meaning \"sprite\"."
  << std::endl;
}
/******************************************************************************/
int main(int argc, char** argv) try {
  if (argc < 2) throw ERROR("Usage: fay [options] image_file\n       fay --help for options");
  
  const struct option long_ops[] = {
    /* NAME           ARGUMENT           FLAG  SHORTNAME */
    {"width",         required_argument, NULL, 'w'},
    {"escape",        no_argument,       NULL, 'e'}, 
    {"help",          no_argument,       NULL, 'h'},
    {"version",       no_argument,       NULL, 'v'},
    { NULL,           0,                 NULL,  0 }
  };
  const char* short_ops = "w:ehv0";
  
  opterr = 0;
  int c;

  size_t w_opt = 0;
  bool   e_opt = false;
  string_t filename;
  
  while ((c = getopt_long(argc, argv, short_ops, long_ops, nullptr)) != -1) {
    switch (c) {
      case 'w': // --width
        try {
          w_opt = std::stoul(optarg);
        }
        catch (...) {
          throw ERROR(string_t("\"") + optarg + "\" is not a valid positive integer.");
        }
        break;
      case 'e': // --escape
        e_opt = true;
        break;
      case 'h': // --help
        printhelp();
        return 0;
      case 'v': // --version
        printf("Fay, terminal image rasteriser, build %s\n", __DATE__);
        return 0;
      case '?':
        throw ERROR("Run 'fay --help' for help.");
      case 0:
      default:
        throw ERROR("Error parsing command line options.");
    }
  }
  
  if (optind < argc) {
    filename = argv[optind];
  }
  else {
    throw ERROR("Filename not specified.");
  }
  
  // ensure terminal supports 24-bit colour
  if (!e_opt) detectTrueColor();
  
  // open image
  Magick::Image image;
  image.read(filename);
  
  if (w_opt == 0) {
    // get terminal dimensions
    const auto [ term_rows, term_cols ] = getTerminalSize();
  
    // scale image to fit the terminal window
    image.scale(Magick::Geometry(std::min(term_cols-1,   image.columns()),
                                 std::min(term_rows*2-5, image.rows())
    ));
  }
  else {
    // scale the image to width passed as an command line option
    image.scale(Magick::Geometry(std::min(w_opt, image.columns()), image.columns()));
  }
  
  // get new image size
  const size_t width     = image.columns();
  const size_t height_px = image.rows();
  
  // output height in characters:
  const size_t full_ch = height_px/2;
  const bool   odd_row = height_px%2;

  string_t output;
  
  uint8_t alpha_threshold = 32;
  
  // process pixels into characters
  PixelPairTC pp;
  if (e_opt) {
    pp.CSI = "\\033[";
    pp.NL = "\\n";
  }
  for (size_t r = 0; r < full_ch; ++r) {
    for (size_t c = 0; c < width; ++c) {
      Magick::ColorRGB px_top    = image.pixelColor(c,r*2);
      Magick::ColorRGB px_bottom = image.pixelColor(c,r*2+1);
      double top_alpha    = px_top.alpha();
      double bottom_alpha = px_bottom.alpha();
      auto r0 = std::max((uint8_t)1, (uint8_t)(px_top.red()     *255*top_alpha));
      auto g0 = std::max((uint8_t)1, (uint8_t)(px_top.green()   *255*top_alpha));
      auto b0 = std::max((uint8_t)1, (uint8_t)(px_top.blue()    *255*top_alpha));
      auto r1 = std::max((uint8_t)1, (uint8_t)(px_bottom.red()  *255*bottom_alpha));
      auto g1 = std::max((uint8_t)1, (uint8_t)(px_bottom.green()*255*bottom_alpha));
      auto b1 = std::max((uint8_t)1, (uint8_t)(px_bottom.blue() *255*bottom_alpha));
      if (top_alpha    *255 < alpha_threshold) r0 = g0 = b0 = 0;
      if (bottom_alpha *255 < alpha_threshold) r1 = g1 = b1 = 0;
      pp.set(r0, g0, b0, r1, g1, b1);
      output += pp.get();
    }
    pp.reset();
    output += pp.get() + pp.NL;
  }
  if (odd_row) {
    for (size_t c = 0; c < width; ++c) {
      size_t r = height_px - 1;
      Magick::ColorRGB px = image.pixelColor(c,r*2);
      double alpha = px.alpha();
      auto r0 = std::max((uint8_t)1, (uint8_t)(px.red()     *255*alpha));
      auto g0 = std::max((uint8_t)1, (uint8_t)(px.green()   *255*alpha));
      auto b0 = std::max((uint8_t)1, (uint8_t)(px.blue()    *255*alpha));
      if (alpha*255 < alpha_threshold) r0 = g0 = b0 = 0;
      pp.set(r0, g0, b0, 0, 0, 0);
      output += pp.get();
    }
    pp.reset();
    output += pp.get() + pp.NL; 
  }
  std::cout << output;
  return 0;
}
catch (ERROR &E) {
  std::cerr << E.what() << std::endl;
  return 1;
}
catch (Magick::Exception &E) { 
  std::cout << "ImageMagick exception: " << E.what() << std::endl; 
  return 2; 
}
